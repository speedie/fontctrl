#!/bin/sh
# fontctrl
# Minimal font manager for GNU/Linux.
# https://codeberg.org/speedie/fontctrl
#
# Copyright (C) 2022 speedie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# hardcoded base directories, can however be overriden by exporting these
[ -z "$FONTCTRL_BASEDIR_USER" ]   && FONTCTRL_BASEDIR_USER="$HOME/.config/fontctrl"
[ -z "$FONTCTRL_BASEDIR_GLOBAL" ] && FONTCTRL_BASEDIR_GLOBAL="/etc/fontctrl"
[ -z "$FONTCTRL_VER" ]            && FONTCTRL_VER="1.2"
[ -z "$GLOBALFONTDIR" ]           && GLOBALFONTDIR="/usr/share/fonts/fonts"
[ -z "$USERFONTDIR" ]             && USERFONTDIR="$HOME/.local/share/fonts"

# wrapper functions to clean up codebase
DIE() { STDERR "$@"; exit 1; }
MSG() { [ -n "$*" ] && printf "%s\n" "$@"; }
MSGN() { [ -n "$*" ] && printf "%s" "$@"; }
STDERR() { MSG "$@" >&2; }
ISNARG() { echo "$CARG" | grep -qE "$COPT|--global|--user|--raw" && return 1 || return 0; }

# help argument
LIST_ARGS() {
		MSG "fontctrl - minimal font manager for POSIX compliant shells."
        MSG "usage: [STDIN] fontctrl [install FONT] [remove FONT] [list] [enable FONT] [disable FONT] [license] [help] [version] [arg]"
        MSG "--global perform the action as a superuser."
        MSG "--user   perform the action as the current user."
        MSG "--raw    print version without any extra information."
}

# create base for fontctrl
CREATE_BASE() {
		# create base directories where font symlinks and configurations for fonts will be placed.
		[ "$(id -u)" = "0" ] && \
            mkdir -p $FONTCTRL_BASEDIR_GLOBAL/fonts && isroot=true; \
            mkdir -p "$FONTCTRL_BASEDIR_USER/fonts"

        echo "$ARG" | grep -q "global" && isglobal=true

		FAIL_NOROOT() {
            [ -z "$isroot" ] || return
			DIE "This action must be used as root."
		}

	    echo "$ARG" | grep -q "global" && FAIL_NOROOT
}

# function to parse arguments and install fonts
INSTALL_FONT() {
    EXITCODE=0 # exit code
    [ -z "$2" ] && DIE "You must specify a font to install."

    # loop through arguments
    for i in $(seq "$ARGC"); do
        CARG="$(printf "%s\n" "$@" | sed -n "${i}","${i}"p)"
        COPT=install
        ISNARG || continue
        [ ! -e "$CARG" ] && EXITCODE=1 && STDERR "Font '$CARG' does not exist." && continue || ARG2="$CARG"
		[ ! -e "$CARG" ] && DIE "Failed to install '$CARG': Not a valid font, exiting." || MSG "Installing font '$CARG'."

        # install the font
        [ "$isroot" != "true" ] && cp -f "$ARG2" "$FONTCTRL_BASEDIR_USER/fonts/" && MSG "Font '$ARG2' installed successfully for $(whoami)." && hi=true && continue
		[ "$isroot" = "true" ]  && cp -f "$ARG2" "$FONTCTRL_BASEDIR_GLOBAL/fonts/" && MSG "Font '$ARG2' installed globally successfully." && hi=true && continue
    done

    [ "$hi" != "true" ] && DIE "Font does not exist."
    [ "$ARG3" != "--global" ] && MSG "Run fontctrl list --user followed by fontctrl enable <font> --user to enable the font." || MSG "Run fontctrl list --global followed by fontctrl enable <font> --global to enable the font."

     return $EXITCODE # return the exit code
}

# function to parse arguments and remove fonts
REMOVE_FONT() {
        EXITCODE=0 # exit code
        [ -z "$2" ] && DIE "You must specify a font to remove."

        for i in $(seq "$ARGC"); do
            CARG="$(printf "%s\n" "$@" | sed -n "${i}","${i}"p)"
            COPT=remove
            ISNARG || continue
            [ -z "$ARG2" ] && MSG "You must specify a font to remove." && exit 1

            # remove globally
            if [ "$isglobal" = "true" ]; then
                    SELFONT="$(find "$FONTCTRL_BASEDIR_GLOBAL"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                    [ ! -e "$SELFONT" ] && EXITCODE=1 && STDERR "No fonts matching '$CARG' found." && continue
                    MSG "Removing font '$SELFONT' globally."
                    hi=true
                    rm -f "$FONTCTRL_BASEDIR_GLOBAL/fonts/$SELFONT" && MSG "Font '$SELFONT' removed successfully. Run fontctrl list to see available fonts."
            else # for the user
                    SELFONT="$(find "$FONTCTRL_BASEDIR_USER"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                    [ ! -e "$SELFONT" ] && EXITCODE=1 && STDERR "No fonts matching '$CARG' found." && continue
                    MSG "Removing font '$SELFONT' for $(whoami)"
                    hi=true
                    rm -f "$SELFONT" && MSG "Font '$SELFONT' removed successfully. Run fontctrl list to see available fonts."
            fi
        done

        [ "$hi" != "true" ] && DIE "Font does not exist."
        return $EXITCODE # return the exit code
}

# function to parse arguments and enable fonts
ENABLE_FONT() {
        EXITCODE=0 # exit code
        [ -z "$2" ] && DIE "You must specify a font to enable."

        for i in $(seq "$ARGC"); do
            CARG="$(printf "%s\n" "$@" | sed -n "${i}","${i}"p)"
            COPT=enable
            ISNARG || continue

            if [ "$isglobal" != "true" ]; then # user
                SELFONT="$(find "$FONTCTRL_BASEDIR_USER"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                BASESELFONT="$(basename "$SELFONT")"
                [ ! -e "$SELFONT" ] && EXITCODE=1 && STDERR "No fonts matching '$CARG' found." && continue
				MSG "Enabling font '$BASESELFONT' for $(whoami)"
				mkdir -p "$USERFONTDIR/fontctrl-fonts"
				rm -f "$USERFONTDIR/fontctrl-fonts/$BASESELFONT"
                hi=true
				[ -e "$FONTCTRL_BASEDIR_USER/fonts/$BASESELFONT" ] && ln -s "$FONTCTRL_BASEDIR_USER/fonts/$BASESELFONT" "$USERFONTDIR/fontctrl-fonts/$BASESELFONT" && MSG "Font '$BASESELFONT' enabled successfully." && continue
                exit 1
            else # global
                SELFONT="$(find "$FONTCTRL_BASEDIR_GLOBAL"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                BASESELFONT="$(basename "$SELFONT")"
                [ ! -e "$SELFONT" ] && STDERR "No fonts matching '$CARG' found." && EXITCODE=1 && continue
				MSG "Enabling font '$BASESELFONT' globally."
				mkdir -p "$GLOBALFONTDIR/fontctrl-fonts"
				rm -f "$GLOBALFONTDIR/fontctrl-fonts/$BASESELFONT"
                hi=true
				[ -e "$FONTCTRL_BASEDIR_GLOBAL/fonts/$SELFONT" ] && ln -s "$FONTCTRL_BASEDIR_GLOBAL/fonts/$SELFONT" "$GLOBALFONTDIR/fontctrl-fonts/$BASESELFONT" && MSG "Font '$SELFONT' enabled successfully." && continue
                exit 1
            fi
        done

        [ "$hi" != "true" ] && DIE "Font does not exist."
        return $EXITCODE # return the exit code
}

# function to parse arguments and disable fonts
DISABLE_FONT() {
        EXITCODE=0 # exit code
        [ -z "$2" ] && DIE "You must specify a font to disable."

        for i in $(seq "$ARGC"); do
            CARG="$(printf "%s\n" "$@" | sed -n "${i}","${i}"p)"
            COPT=disable
            ISNARG || continue

            if [ "$isglobal" != "true" ]; then # user
                SELFONT="$(find "$FONTCTRL_BASEDIR_USER"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                BASESELFONT="$(basename "$SELFONT")"
                [ ! -e "$SELFONT" ] && EXITCODE=1 && STDERR "No fonts matching '$CARG' found." && continue
				MSG "Disabling font '$BASESELFONT' for $(whoami)"
                hi=true
				rm -f "$USERFONTDIR/fontctrl-fonts/$BASESELFONT" && MSG "Font '$SELFONT' disabled successfully." && continue
				exit 1
            else # global
                SELFONT="$(find "$FONTCTRL_BASEDIR_GLOBAL"/fonts/*"$CARG"* -type f 2>/dev/null | head -n 1)" # Head in case there are multiple matches
                BASESELFONT="$(basename "$SELFONT")"
                [ ! -e "$SELFONT" ] && EXITCODE=1 && STDERR "No fonts matching '$CARG' found." && continue
				MSG "Disabling font '$BASESELFONT' globally."
                hi=true
				rm -f "$GLOBALFONTDIR/fontctrl-fonts/$BASESELFONT" && MSG "Font '$BASESELFONT' disabled successfully." && continue
                exit 1
            fi
        done

        [ "$hi" != "true" ] && DIE "Font does not exist."
        return $EXITCODE # return the exit code
}

# list fonts
LIST_FONT() {
		MSG "List of all installed fonts:"
        [ "$isglobal" = "true" ] && basename "$(find "$FONTCTRL_BASEDIR_GLOBAL/fonts/" -type f)" || find "$FONTCTRL_BASEDIR_USER/fonts/" -type f -printf '%f\n'
		MSG "Use fontctrl enable/disable to enable/disable a font. See fontctrl help for more information." && return || exit 1
}

# print version information
PRINT_VER() {
        [ -z "$ARG3" ] || DIE "This option does not take a third argument."
        [ -n "$ARG2" ] && [ "$ARG2" != "--raw" ] && DIE "Invalid argument: '$ARG2'"
        [ "$ARG2" != "--raw" ] && MSG "fontctrl v$FONTCTRL_VER" || MSG "$FONTCTRL_VER"
}

# print license information
PRINT_LICENSE() {
        [ -z "$ARG2" ] && MSG "fontctrl is free software, licensed under the GNU General Public License version 3.0. See https://codeberg.org/speedie/fontctrl for more information." || DIE "This option does not take an argument."
}

# set everything up
INIT() {
    # convenient variables
    ARG="$*"
    ARG1="$1"
    ARG2="$2"
    ARG3="$3"

    CREATE_BASE "$@"

    # arg count, this is used many times so why not set it here
    ARGC="$(printf "%s\n" "$@" | wc -l)"

    case "$ARG1" in
        "install") ARG=recognized && INSTALL_FONT  "$@" && exit 0 || exit 1 ;;
        "remove")  ARG=recognized && REMOVE_FONT   "$@" && exit 0 || exit 1 ;;
        "enable")  ARG=recognized && ENABLE_FONT   "$@" && exit 0 || exit 1 ;;
        "disable") ARG=recognized && DISABLE_FONT  "$@" && exit 0 || exit 1 ;;
        "list")    ARG=recognized && LIST_FONT     "$@" && exit 0 || exit 1 ;;
        "help")    ARG=recognized && LIST_ARGS     "$@" && exit 0 || exit 1 ;;
        "version") ARG=recognized && PRINT_VER     "$@" && exit 0 || exit 1 ;;
        "license") ARG=recognized && PRINT_LICENSE "$@" && exit 0 || exit 1 ;;
        "")        ARG=recognized && LIST_ARGS     "$@" && exit 0 || exit 1 ;;
    esac

    [ "$ARG" != "recognized" ] && MSG "Invalid argument: '$ARG1'" && DIE "Run fontctrl help or with no arguments for help."
}

# take stdin
[ -p /proc/self/fd/0 ] && read STDIN && set -- "$@" $STDIN

INIT "$@"
# ignore 2015, i don't think this will be a problem.
# shellcheck disable=SC2015
